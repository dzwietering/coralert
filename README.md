# Positive testing alert dashboard

Using the measure of average positives over the past week divided by the average positives over the two weeks before.

[Dutch alert radar by municipality](https://dzwietering.gitlab.io/coralert/zzcoralert.html)
